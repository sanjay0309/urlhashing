<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Url;
use Illuminate\Support\Facades\Hash;
use App;
use Base62\Base62;


class UrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'original_url'=>'required',
        ]);

	

        $base62 = gmp_strval( gmp_init( bin2hex($request->get('original_url')), 16), 62);

        $hash =  substr($base62, 0, 8);

        $link = "http://".$request->getHttpHost()."/".$hash;


        if (Url::where('hash', $hash)->exists()) {
            return view('url',compact('link'));
        }


        $url = new Url([
            'original_url' => $request->get('original_url'),
            'hash' => $hash
        ]);
        $saved = $url->save();       

        return view('url',compact('link'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function shortenLink($code)
    {
        $find = Url::where('hash', strval($code))->first();
   
        return redirect($find->original_url);
    }
}
