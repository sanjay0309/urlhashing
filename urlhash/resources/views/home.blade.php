@extends('layouts.app')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full sm:px-6">

        @if (session('status'))
            <div class="text-sm border border-t-8 rounded text-green-700 border-green-600 bg-green-100 px-3 py-4 mb-4" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <section class="flex flex-col break-words bg-white sm:border-1 sm:rounded-md sm:shadow-sm sm:shadow-lg">

            <header class="font-semibold bg-gray-200 text-gray-700 py-5 px-6 sm:py-6 sm:px-8 sm:rounded-t-md">
                Dashboard
            </header>

            <div class="w-full p-6">
            <form class="w-full px-6 space-y-6 sm:px-10 sm:space-y-8" method="POST" action="urls">
                    @csrf

                    <div class="flex flex-wrap">
                        <label for="original_url" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('Url Address') }}:
                        </label>

                        <input id="original_url" type="original_url"
                            class="form-input w-full @error('original_url') border-red-500 @enderror" name="original_url"
                            value="{{ old('original_url') }}" required autocomplete="original_url" autofocus>

                        @error('original_url')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>

                    

                    <div class="flex flex-wrap">
                        <button type="submit"
                        class="w-full select-none font-bold whitespace-no-wrap p-3 rounded-lg text-base leading-normal no-underline text-gray-100 bg-blue-500 hover:bg-blue-700 sm:py-4">
                        {{ __('Get Sort Url') }}
                        </button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</main>
@endsection
